﻿# This script is allows you to create VM's in a Virtual Box

Write-Host "Please answer the question bellow"

# Setting a variables for next steps

$vmname = Read-Host 'Enter VMname, example = test-vm'
$ram = Read-Host 'Enter number of RAM, example = 4096'
$network = "bridged" # example = none|null|nat|natnetwork|bridged|intnet|hostonly|generic
$vram = 128
$cpu = Read-Host 'Enter number of CPU, example = 1'
$hdd = 50000 # example = 50000 = 50gb
$isofolder = 'H:\Datastore'
$vmfolder = 'H:\vDC'


Write-Host "Please ensure that isofolder is correct for you"
Write-Host "That is you path to iso $isofolder"


$checkiso = Read-Host "Enter yes/no"

If ($checkiso -eq "no"){
    $isofolder = Read-Host "Enter correct path to iso"
}

Write-Host "Retrieves a list of supported OSes"


VBoxManage list ostypes | findstr "ID" | findstr -V "Family"


$ostype = Read-Host "Copy ostype from list above"

Write-Host "Retrives a list of availabe iso on the host"

Get-ChildItem -Path $isofolder -File -rec -Name "*.iso"

$iso = Read-Host "Copy iso from list above"

Write-Host "Please ensure that vmfolder is correct for you"
Write-Host "That is you path to vmfolder $vmfolder"

$checkvm = Read-Host "Enter yes/no"

If ($checkvm -eq "no"){
    $vmfolder = Read-Host "Enter correct path to vmfolder"
}

Write-Host "If you done everything right, the setup will begin in few seconds. Good luck!"

pause

# Create VM
VBoxManage createvm --name $vmname --ostype $ostype --register --basefolder $vmfolder 

# Set memory, video, cpu and network
VBoxManage modifyvm $vmname --ioapic on
VBoxManage modifyvm $vmname --memory $ram --vram $vram
VBoxManage modifyvm $vmname --cpus $cpu
VBoxManage modifyvm $vmname --nic1 $network --bridgeadapter1 "Intel(R) Ethernet Connection (7) I219-V"
VBoxManage modifyvm $vmname --graphicscontroller vmsvga


# Setting paths for disk one more time

Write-Host "Setting paths for disks one more time"

$diskpath = "$vmfolder\$vmname\$vmname"+"_DISK.vdi"
$isofullpath = "$isofolder\$iso"

# Create Disk and connect Iso
VBoxManage createhd --filename $diskpath --size $hdd --format VDI
VBoxManage storagectl $vmname --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach $vmname --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium  $diskpath
VBoxManage storagectl $vmname --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach $vmname --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $isofullpath
VBoxManage modifyvm $vmname --boot1 dvd --boot2 disk --boot3 net --boot4 none

#Start the VM
VBoxManage startvm $vmname --type headless

exit

Write-Host "VM successfully created"
