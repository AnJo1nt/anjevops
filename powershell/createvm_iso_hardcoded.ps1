﻿# This script is allows you to create VM's in a Virtual Box

Write-Host "Please answer the question bellow"

# Setting a variables for next steps

$vmname = Read-Host 'Enter VMname, example = test-vm'
$ram = Read-Host 'Enter number of RAM, example = 4096'
$network = "bridged" # example = none|null|nat|natnetwork|bridged|intnet|hostonly|generic
$vram = "128"
$cpu = Read-Host 'Enter number of CPU, example = 1'
$hdd = "50000" # example = 50000 = 50gb
$isofolder = 'H:\Datastore'
$vmfolder = 'H:\vDC'
$iso = "CentOS-8.1.1911-x86_64-dvd1.iso"
$ostype = "RedHat_64"

# Create VM
VBoxManage createvm --name $vmname --ostype $ostype --register --basefolder $vmfolder 

# Setting VBOx variable

$vboxpath = "$vmfolder\$vmname\$vmname"+".vbox"

# Register VM

VBoxManage registervm $vboxpath

# Set memory, video, cpu and network
VBoxManage modifyvm $vmname --ioapic on
VBoxManage modifyvm $vmname --memory $ram --vram $vram
VBoxManage modifyvm $vmname --cpus $cpu
VBoxManage modifyvm $vmname --nic1 $network --bridgeadapter1 "Intel(R) Ethernet Connection (7) I219-V"
VBoxManage modifyvm $vmname --graphicscontroller vmsvga

# Setting paths for disk one more time

$diskpath = "$vmfolder\$vmname\$vmname"+"_DISK.vdi"
$isofullpath = "$isofolder\$iso"

# Create Disk and connect Iso
VBoxManage createhd --filename $diskpath --size $hdd --format VDI
VBoxManage storagectl $vmname --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach $vmname --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium  $diskpath
VBoxManage storagectl $vmname --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach $vmname --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $isofullpath
VBoxManage modifyvm $vmname --boot1 dvd --boot2 disk --boot3 net --boot4 none

#Start the VM
# VBoxManage startvm $vmname --type headless

exit