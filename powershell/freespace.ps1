# This code help u to find freespace in gb on Windows System with powershell
# U can delete Filter block, to check all drives
# U can delete $FreeSpaceInt to avoid conversation of double to int
# Write-Host $FreeSpaceInt or $FreeSpaceInt - output on screen

$diskC = gwmi win32_logicaldisk  -Filter "DeviceID='C:'" | select @{n="FreeSpace";e={[math]::Round($_.FreeSpace/1GB,2)}}
$FreeSpace = $diskC | select -ExpandProperty FreeSpace
$FreeSpaceInt = [int]$FreeSpace

# Write-Host $FreeSpaceInt or $FreeSpaceInt - output on screen


# Basic condition to check functionality
If ($FreeSpaceInt -gt 5){
    Write-Host "Enough space for updates"
} else {
    Write-Host "Not enough space for updates"    
}
