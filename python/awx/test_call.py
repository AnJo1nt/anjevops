import requests
import os
from requests.auth import HTTPBasicAuth
import json
import yaml


def import_variables_from_file():
 my_variables_file=open('./python/awx/variables.yml', 'r')
 my_variables_in_string=my_variables_file.read()
 # print my_variables_in_string
 my_variables_in_yaml=yaml.load(my_variables_in_string)
 # print my_variables_in_yaml
 # print my_variables_in_yaml['awx']['ip']
 my_variables_file.close()
 return my_variables_in_yaml

def get_the_project_id(): 
 url = url_base + "/api/v2/projects/"
 rest_call = requests.get(url, headers=headers, auth=(authuser, authpwd))
 # pprint (rest_call.json())
 for item in rest_call.json()['results']:
  if item['name'] == my_variables_in_yaml['project']['name']:
 #  print "Project id is " + str(item['id'])
   project_id = str(item['id'])
 return project_id

authuser = 'admin'
authpwd = 'password'
headers = {'content-type': 'application/json'}
my_variables_in_yaml=import_variables_from_file()
url_base = 'http://' + my_variables_in_yaml['awx']['ip']

project_id = get_the_project_id()

print (project_id)

