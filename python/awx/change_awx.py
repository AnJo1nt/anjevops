import requests
from requests.auth import HTTPBasicAuth
import json
from pprint import pprint
import yaml
import time



##################################################
# This block defines the functions we will use
###################################################
def import_variables_from_file():
 my_variables_file=open('variables.yml', 'r')
 my_variables_in_string=my_variables_file.read()
 # print my_variables_in_string
 my_variables_in_yaml=yaml.load(my_variables_in_string)
 # print my_variables_in_yaml
 # print my_variables_in_yaml['awx']['ip']
 my_variables_file.close()
 return my_variables_in_yaml

def get_the_project_id(): 
 url = url_base + "/api/v2/projects/"
 rest_call = requests.get(url, headers=headers, auth=(authuser, authpwd))
 # pprint (rest_call.json())
 for item in rest_call.json()['results']:
  if item['name'] == my_variables_in_yaml['project']['name']:
 #  print "Project id is " + str(item['id'])
   project_id = str(item['id'])
 return project_id

def update_project_branch():
 url = url_base + "/api/v2/projects/" + my_variables_in_yaml['projects']['id'] + "/"
 payload = {
  "name": my_variables_in_yaml['project']['name'],
  "scm_branch": my_branch,
  "allow_override": True
 }
 rest_call = requests.patch(url, data=json.dumps(payload), headers=headers, auth=(authuser, authpwd))
 print (rest_call.status_code) 

 if rest_call.status_code == 200:
   print ("Project successfully updated")
 else:
   print ("Failed to update the project")

def update_project_for_template():
 url = url_base + "/api/v2/job_templates/" + my_variables_in_yaml['templates']['id'] + "/"
 payload = {
  "name": my_variables_in_yaml['template']['name'],
  "project": my_project
 }
 rest_call = requests.patch(url, data=json.dumps(payload), headers=headers, auth=(authuser, authpwd))
 print (rest_call.status_code)
 print (rest_call.text)

 if rest_call.status_code == 200:
   print ("Template successfully updated")
 else:
   print ("Failed to update the Template")


# start configuration
my_variables_in_yaml=import_variables_from_file()
# this is the default AWX user
authuser = 'admin'
authpwd = 'password'
headers = { 'content-type' : 'application/json' }
url_base = 'http://' + my_variables_in_yaml['awx']['ip']

project_id = get_the_project_id()

# print('Введите имя ветки')
# my_branch = input()

# my_project = "8"

# update_project_for_template()

# update_project_branch()


