import requests
import urllib3
import os
import sys

from common_procedures import send_request, create_payload


def linux_patching_payload(payload_proc):
"""Adds Installation type and Vault token to request's payload."""
def wrapper(**kwargs):
option2 = kwargs.pop('option2', '')

categories = os.environ.get('Linux Categories', '')

фывафы

payload = payload_proc(option2=option2)



if categories == 'all':
    payload['updateParameters'] = {'updateAll': True}
elif categories == 'security':
    payload['updateParameters'] = {'security': True}


return payload

return wrapper


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


if __name__ == "__main__":

create_payload = linux_patching_payload(create_payload)

try:
send_request(func=create_payload, endpoint='/sms/v2/update/linux')
except requests.RequestException as e:
print("REST call failure")
print(e)
sys.exit(1)