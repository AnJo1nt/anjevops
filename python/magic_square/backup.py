from tabulate import tabulate
import random

# basic digits

a = 2
b = 3
c = 4


# formula

def calculations(a, b, c):
    # row 1

    col1_1 = a + b
    col2_1 = a + 2 * b + 2 * c
    col3_1 = a + c

    # row 2

    col1_2 = a + 2 * c
    col2_2 = a + b + c
    col3_2 = a + 2 * b

    # row 3

    col1_3 = a + 2 * b + c
    col2_3 = a
    col3_3 = a + b + 2 * c

    # squaring

    col1_1 = pow(col1_1, 2)
    col2_1 = pow(col2_1, 2)
    col3_1 = pow(col3_1, 2)
    col1_2 = pow(col1_2, 2)
    col2_2 = pow(col2_2, 2)
    col3_2 = pow(col3_2, 2)
    col1_3 = pow(col1_3, 2)
    col2_3 = pow(col2_3, 2)
    col3_3 = pow(col3_3, 2)

    # row sums

    row1_sum = sum([col1_1, col2_1, col3_1])
    row2_sum = sum([col1_2, col2_2, col3_2])
    row3_sum = sum([col1_3, col2_3, col3_3])

    # col sums

    col1_sum = sum([col1_1, col1_2, col1_3])
    col2_sum = sum([col2_1, col2_2, col2_3])
    col3_sum = sum([col3_1, col3_2, col3_3])
    diag1_sum = sum([col1_1, col2_2, col3_3])
    diag1_sum = 'Diag1 ' + str(diag1_sum)
    diag2_sum = sum([col1_3, col2_2, col3_1])
    diag2_sum = 'Diag2 ' + str(diag2_sum)
    table = [['title', 'col1', 'col2', 'col3', diag2_sum], ['row1', col1_1, col2_1, col3_1, row1_sum],
             ['row2', col1_2, col2_2, col3_2, row2_sum], ['row3', col1_3, col2_3, col3_3, row3_sum],
             ['col sums', col1_sum, col2_sum, col3_sum, diag1_sum]]

    return table

    while true:
        if row1_sum == row2_sum == row3_sum:
           break
        a = random.randint(0,100)
        b = random.randint(0,100)
        c = random.randint(0,100)



print(tabulate(calculations(a, b, c)))