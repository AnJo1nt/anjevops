Jenkins installation on k8s

1. Install jenkins master
2. Go to Jenkins UI (node_ip:30000)
3. Install plugins
4. Create cloud provider k8s
5. Fill in the fields:

- Kubernetes options
Name: kubernetes
Kubernetes URL: https://kubernetes.default:443
Kubernetes namespace: jenkins
Jenkins URL: http://jenkins:8080 # where jenkins is the name of service
Jenkins tunnel: jenkins-jnlp:50000 # where jenkins-jnlo is the of service
Pod Label { 
    jenkins:slave 
    }
- Pod Template
Name: jenkins-slave
Namespace: jenkins
Labels: jenkins-slave
- Container Template
Name: jnlp
Docker-image: aimvector/jenkins-slave
Working-directory: /home/jenkins/agent
Allocate pseudo-TTY: yes
- Volumes
host path: /var/run/docker.sock
mount path: /var/run/docker.sock
- Raw yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    jenkins: slave
    jenkins/label: jenkins-slave
  namespace: jenkins
spec:
  securityContext:
    runAsUser: 0 # this is important, without it, Docker daemon socket always getting permission denied